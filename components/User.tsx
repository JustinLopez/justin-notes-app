import { createClient } from '@/utils/supabase/server';
import { redirect } from 'next/navigation';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faArrowRightFromBracket,
} from "@fortawesome/free-solid-svg-icons";


export default async function User() {
	const supabase = createClient();

	const {
		data: { session },
	} = await supabase.auth.getSession();

	const signOut = async () => {
		'use server';

		const supabase = createClient();
		await supabase.auth.signOut();
		return redirect('/login');
	};
	
	return (
		session && (
			<div className='flex items-center gap-4'>
				<span className='text-sm font-bold text-gray-500'>Hi, {session.user.email!.split('@')[0]}</span>
				<form action={signOut}>
					<button className='py-2 px-4 rounded-md no-underline bg-btn-background hover:bg-btn-background-hover'>
					<FontAwesomeIcon icon={faArrowRightFromBracket} />
					</button>
				</form>
			</div>
		)
	);
}