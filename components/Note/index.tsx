import { Note } from "@/models/interfaces";
import { deleteNote } from "@/app/lib/actions/delete-notes";
import { faTrash, faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Note({ note }: { note: Note }) {
  return (
      <div className="relative p-6 border rounded-lg shadow bg-white overflow-y-auto flex flex-row justify-between items-center">
        <header
          className="flex items-center justify-between gap-3 sticky top-0"
          id="title-note"
        >
          <h5
            id="title-note"
            className="text-xl font-bold tracking-tight text-gray-500 sticky top-0 text-balance pr-10"
          >
            {note.title}
          </h5>
          <div className="flex items-center text-gray-500">
          <span className="text-gray-500 pr-4">
            <FontAwesomeIcon icon={faPenToSquare} />
            </span>
            <form action={deleteNote}>
              <input type="hidden" name="id" value={note.id} />
              <button type="submit" className="">
                <FontAwesomeIcon icon={faTrash} />
              </button>
           
            </form>
          </div>
        </header>
      </div>
  );
}

export default Note;
