import NoteForm from './NoteForm';

function AddNote() {
	return (
		<section className='border-b border-b-gray-400 mb-10'>
			<h2 className='text-white text-4xl text-center flex justify-center xl:text-left font-semibold tracking-wide my-10 uppercase'>Add new Note</h2>
			<NoteForm></NoteForm>
		</section>
	);
}

export default AddNote;