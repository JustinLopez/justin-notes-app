'use client';

import { useState } from 'react';
import { updateNote } from '@/app/lib/actions/update-note';
import { Note } from '@/models/interfaces';
import {  faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function EditNote({ note }: { note: Note }) {
	const [showModal, setShowModal] = useState(false);
	const [formData, setFormData] = useState({
		title: note.title,
	});

	const handleChange = (e: any) => setFormData({ ...formData, [e.target.name]: e.target.value });

	const renderModal = () => {
		if (!showModal) return;

		return (
			<div className=' w-full bg-gray-800 bg-opacity-75 flex justify-center items-center px-4 overflow-visible'>
				<div className='bg-gray-900 p-6 rounded-lg w-full'>
					<span
						className='text-white text-xl leading-none hover:text-gray-300 cursor-pointer float-right'
						onClick={() => setShowModal(false)}>
						&times;
					</span>
					<form action={updateNote} onSubmit={() => setShowModal(false)} className='mt-4'>
						<input type='hidden' name='id' value={note.id} />
						<div className='mb-4'>
							<label htmlFor='title' className='block text-gray-300 mb-2'>
								Title
							</label>
							<input
								type='text'
								id='title'
								name='title'
								value={formData.title}
								onChange={handleChange}
								className='w-full p-2 rounded bg-gray-800 text-white border border-gray-700 focus:border-blue-500'
							/>
						</div>
						<button
							type='submit'
							className='w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'>
							Update Note
						</button>
					</form>
				</div>
			</div>
		);
	};

	return (
		<div className={`${showModal ? '' : ''}`}>
			<button onClick={() => setShowModal(true)} className=''>
            <FontAwesomeIcon icon={faPenToSquare} />
			</button>
			{renderModal()}
		</div>
	);
}