"use client";

import { addNote } from "@/app/lib/actions/add-notes";
import { ToastContainer, toast } from "react-toastify";

export default function WatchForm() {

  const handleSubmit = async (event: FormData) => {
    try {
      await addNote(event);
      toast.success("Note Added");
    } catch (error: any) {
      console.log(error);
      toast.error(error.message);
    }
  };

  return (
    <form
      action={(event) => handleSubmit(event)}
      className="mb-6 max-w-[400px] bg-white p-10 rounded-lg"
    >
      <ToastContainer autoClose={2500} position="top-right" />

      <div className="mb-4 ">
        <label htmlFor="title" className="block text-gray-500 mb-2">
          Note title
        </label>
        <input
          type="text"
          id="title"
          name="title"
          placeholder="Type your Note" 
          className="shadow appearance-none border border-gray-400 rounded w-full py-2 px-3 text-gray-400 focus:outline-none"
          required
        />
      </div>

      <button
        type="submit"
        className="text-white  bg-gradient-to-r from-cyan-500 to-blue-500 font-medium py-2 px-4 rounded"
      >
        Add Note
      </button>
    </form>
  );
}
