import Link from 'next/link';
import User from '../User';
import { createClient } from '@/utils/supabase/server';

export default async function Header() {
	const supabase = createClient();

	const {
		data: { session },
	} = await supabase.auth.getSession();

	return (
		<div className='flex flex-row items-center justify-between w-full bg-white p-5 rounded-b-md shadow-md'>
			<Link href='/' className='font-bold text-lg text-gray-500 text-center'>
				 Notes App
			</Link>
			<User />

			{!session && (
				<Link
					href='/'
					className='py-2 px-4 rounded-md no-underline text-foreground bg-btn-background hover:bg-btn-background-hover text-sm m-4'>
					Home
				</Link>
			)}
		</div>
	);
}