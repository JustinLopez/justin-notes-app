export interface Note {
	id: number;
	created_at: string;
	title: string;
	user_id: string;
}