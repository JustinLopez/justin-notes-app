import Header from "@/components/Header/";
import AddNote from "@/components/Note/AddNote";
import NoteComponent from "@/components/Note/index";
import { Note } from "@/models/interfaces";
import { createClient } from "@/utils/supabase/server";
import { redirect } from "next/navigation";

export default async function Notes() {
  const supabase = createClient();
  const {
    data: { session },
  } = await supabase.auth.getSession();
  const user = session?.user;

  if (!user) return redirect("/login");

  const { data: notes, error } = await supabase
    .from("notes")
    .select("*")
    .eq("user_id", user.id);

  if (error) {
    console.error("Error fetching Notes");
  }

  const renderNotes = () => {
    return notes?.map((note: Note) => (
      <li className="max-w-[360px]" key={note.id}>
        <NoteComponent note={note} key={note.id} />
      </li>
    ));
  };

  return (
    <>
      <Header />
      <AddNote />
      <section>
        <ul className="grid grid-cols-1 place-items-center md:grid-cols-2 xl:grid-cols-3 gap-14">
          {renderNotes()}
        </ul>
      </section>
    </>
  );
}
