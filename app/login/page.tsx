import Header from '@/components/Header';
import { createClient } from '@/utils/supabase/server';
import Link from 'next/link';
import { redirect } from 'next/navigation';

export default async function Login({ searchParams }: { searchParams: { message: string } }) {
	const supabase = createClient();

	const {
		data: { session },
	} = await supabase.auth.getSession();

	if (session) {
		return redirect('/');
	}

	const signIn = async (formData: FormData) => {
		'use server';

		const email = formData.get('email') as string;
		const password = formData.get('password') as string;
		const supabase = createClient();

		const { error } = await supabase.auth.signInWithPassword({
			email,
			password,
		});

		if (error) {
			return redirect('/login?message=Could not authenticate user');
		}

		return redirect('/notes');
	};

	return (
		<div>
			<Header />

			<div className='w-full bg-white p-8 rounded-lg sm:max-w-md mx-auto mt-4 shadow-lg'>
				<form
					className='animate-in flex-1 flex flex-col w-full justify-center gap-2 text-foreground mb-4'
					action={signIn}>
					<label className='text-md text-gray-600' htmlFor='email'>
						Email
					</label>
					<input
						className='rounded-md px-4 py-2 bg-inherit border mb-6 focus:outline-none'
						name='email'
						placeholder='you@example.com'
						required
					/>
					<label className='text-md text-gray-600' htmlFor='password'>
						Password
					</label>
					<input
						className='rounded-md px-4 py-2 bg-inherit border mb-6 focus:outline-none'
						type='password'
						name='password'
						placeholder='Type your password'
						required
					/>
					<button className='rounded-xl px-4 font-medium bg-gradient-to-r text-white from-cyan-500 to-blue-500 py-2 mb-2 hover:opacity-90 tracking-wide'>
						Login
					</button>

					{searchParams?.message && (
						<p className='mt-4 p-4 bg-foreground/10 text-foreground text-center'>{searchParams.message}</p>
					)}
				</form>

				<br />
				<br />

				<p className='text-gray-500'>
					Don't have an Account?{' '}
					<Link href='/signup' className='text-blue-300 underline underline-offset-4'>
						Sign Up
					</Link>
				</p>
			</div>
		</div>
	);
}